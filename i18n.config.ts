import en from "./i18n/en";
import de from "./i18n/de";

export default defineI18nConfig(() => ({
  legacy: false,
  locale: "en",
  messages: {
    en,
    de,
  },
  strategy: "no_prefix",
  detectBrowserLanguage: {
    useCookie: true,
    cookieKey: "i18n",
  },
  lazy: true,
}));
