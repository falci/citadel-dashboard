import { defineStore } from "pinia";

export interface State {
  name: string;
  jwt: string;
  registered: boolean;
  totpKey: string;
  totpEnabled: boolean;
  totpAuthenticated: boolean;
  seed: string[];
  installedApps: string[];
  letsencryptSettings: {
    email?: string | undefined;
    agreed_lets_encrypt_tos?: boolean | undefined;
    app_domains?: Record<string, string> | undefined;
  };
  runningCitadelSettings: {
    isSetup: boolean;
    username: string;
    password: string;
    subdomain: string;
  };
  ipAddr: string;
}

// Initial state
const useUserStore = defineStore("user", {
  state: (): State => ({
    name: "",
    // @ts-expect-error
    jwt: useCookie("jwt", ""),
    registered: true,
    totpKey: "",
    totpEnabled: false,
    totpAuthenticated: false,
    seed: [],
    installedApps: [],
    letsencryptSettings: {},
    runningCitadelSettings: {
      isSetup: false,
      username: "",
      password: "",
      subdomain: "",
    },
    ipAddr: "",
  }),

  // Functions to get data from the API
  actions: {
    async login({
      password,
      totpToken,
    }: {
      password: string;
      totpToken: string;
    }) {
      const citadel = useCitadel();
      const jwt = await citadel.auth.login(password, totpToken);
      if (jwt) {
        this.setJwt(jwt);
      }
    },

    logout() {
      if (this.jwt) {
        this.setJwt("");
      }
      navigateTo("/");
    },

    setJwt(jwt: string) {
      this.jwt = jwt;
      useCookie("jwt").value = jwt;
    },

    async refreshJWT() {
      const citadel = useCitadel();
      try {
        const jwt = await citadel.auth.refresh();
        if (jwt) {
          this.setJwt(jwt);
        }
      } catch {
        this.setJwt("");
        navigateTo("/");
      }
    },

    async getRegistered() {
      const citadel = useCitadel();
      const registered = await citadel.auth.isRegistered();
      this.registered = registered;
    },

    async getInfo() {
      const citadel = useCitadel();
      const { name, installedApps } = await citadel.auth.info();
      this.name = name;
      this.installedApps = installedApps || [];
    },

    async getTotpKey() {
      const citadel = useCitadel();
      const totpKey = await citadel.auth.setupTotp();
      this.totpKey = totpKey;
    },

    async getTotpEnabledStatus() {
      const citadel = useCitadel();
      const totpEnabled = await citadel.auth.isTotpEnabled();
      this.totpEnabled = totpEnabled;
    },

    async getSeed(auth?: { password: string; totpToken?: string }) {
      const citadel = useCitadel();
      let rawSeed: string[];

      // first check if user is registered or not
      await this.getRegistered();

      // get user's stored seed if already registered
      if (this.registered && auth?.password) {
        rawSeed = await citadel.auth.seed(auth.password, auth.totpToken);
      } else {
        // get a new seed if new user
        rawSeed = await citadel.lightning.wallet.generateSeed();
      }

      if (rawSeed) {
        this.seed = rawSeed;
      }
    },

    async register({
      name,
      password,
      seed,
    }: {
      name: string;
      password: string;
      seed: string[];
    }) {
      const citadel = useCitadel();
      if (!this.registered) {
        const response = await citadel.auth.register(name, password, seed);

        if (response && response.jwt) {
          this.setJwt(response.jwt);
          this.registered = true;
          this.seed = []; // Remove seed from store
        }
      }
    },
    async getRunningCitadelSettings() {
      const citadel = useCitadel();
      this.runningCitadelSettings = await citadel.auth.runningCitadelStatus();
    },
    async getLetsEncryptSettings() {
      const citadel = useCitadel();
      this.letsencryptSettings = await citadel.auth.letsEncryptStatus();
    },
    async enableLetsEncrypt() {
      const citadel = useCitadel();
      if (
        this.letsencryptSettings.agreed_lets_encrypt_tos &&
        !!this.letsencryptSettings.email
      ) {
        await citadel.auth.enableLetsEncrypt(this.letsencryptSettings.email!);
      }
    },
    async getIpAddr() {
      const citadel = useCitadel();
      this.ipAddr = await citadel.auth.ipAddr();
    },
  },
});

export default useUserStore;
