import { BigNumber } from "bignumber.js";
import useSystemStore from "@/store/system";

// Never display numbers as exponents
BigNumber.config({ EXPONENTIAL_AT: 1e9 });

export function btcToSats(input: number) {
  const btc = new BigNumber(input);
  const sats = btc.multipliedBy(100000000);

  if (isNaN(sats as unknown as number)) {
    return 0;
  }

  return Number(sats);
}

export function satsToBtc(input: number, decimals = 8) {
  const sats = new BigNumber(input);
  const btc = sats.dividedBy(100000000);

  if (isNaN(btc as unknown as number)) {
    return 0;
  }

  return Number(btc.decimalPlaces(decimals));
}

export function toPrecision(input: number, decimals = 8) {
  const number = new BigNumber(input);

  if (isNaN(number as unknown as number)) {
    return 0;
  }

  return number.decimalPlaces(decimals).toString();
}

export function unit(value: number | string) {
  const store = useSystemStore();
  store.getUnit();
  if (store.unit === "sats") {
    return Number(value);
  } else if (store.unit === "btc") {
    return satsToBtc(Number(value));
  }
}
export function sats(value: number | string) {
  return Number(value);
}
export function btc(value: number | string) {
  return satsToBtc(Number(value));
}
export function formatUnit(unit: "sats" | "btc") {
  if (unit === "sats") {
    return "Sats";
  } else if (unit === "btc") {
    return "BTC";
  }
}
export function satsToUSD(
  value: string | number,
  price: number,
  currency: string,
): string {
  if (isNaN(parseInt(value.toString()))) {
    return value as string;
  } else {
    return Number(satsToBtc(parseInt(value.toString())) * price).toLocaleString(
      navigator.language,
      {
        style: "currency",
        currency,
      },
    );
  }
}
export function localize(n: number | string) {
  return Number(n).toLocaleString(undefined, { maximumFractionDigits: 8 });
}
