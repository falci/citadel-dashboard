import Citadel from "@runcitadel/sdk-next";

export function useCitadel() {
  let citadel: Citadel;
  if (process.client) {
    citadel = new Citadel(
      useRuntimeConfig().public.baseURL || window.location.origin,
    );
  } else {
    citadel = new Citadel(
      useRuntimeConfig().public.baseURL || "http://host.docker.internal",
    );
  }
  citadel.jwt = useCookie("jwt");
  return citadel;
}
